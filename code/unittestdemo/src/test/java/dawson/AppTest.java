package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
   

    @Test
    public void shouldReturnInputValue(){
        App app = new App();
        assertEquals("testing is input is same as return", 5,app.echo(5) );
    }

    @Test
    public void shouldReturnOneMore(){
        App app = new App();
        assertEquals("testing is input is one more than return", 6,app.oneMore(5) );
    }

}
